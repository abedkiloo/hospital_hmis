from rest_framework import generics

from .models import Employees
from .serializers import EmployeeSerializer


class EmployeesList(generics.ListAPIView):
    """ View to list all Employees"""
    queryset = Employees.objects.all().order_by('first_name')
    serializer_class = EmployeeSerializer


class EmployeesCreate(generics.CreateAPIView):
    """ View to create a new Department. Only accepts POST requests """
    queryset = Employees.objects.all()
    serializer_class = EmployeeSerializer


class EmployeesRetrieveUpdate(generics.RetrieveUpdateAPIView):
    """ Retrieve a Department or update Department information.
    Accepts GET and PUT requests and the record id must be provided in the request """
    queryset = Employees.objects.all()
    serializer_class = EmployeeSerializer


class EmployeesDetail(generics.RetrieveAPIView):
    queryset = Employees.objects.all()
    serializer_class = EmployeeSerializer
