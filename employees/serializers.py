from rest_framework import serializers

from .models import Employees


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employees
        fields = (
        "id", "first_name", "middle_name", "last_name", "phone_number", "id_number", "date_of_birth", "created_at",
        "updated_at", "status")
