from django.db import models

# Create your models here.

class Employees(models.Model):
    first_name = models.CharField(max_length=255, null=False)
    middle_name = models.CharField(max_length=255, null=False)
    last_name = models.CharField(max_length=255, null=False)
    phone_number = models.IntegerField()
    id_number = models.IntegerField()
    department = models.ForeignKey('departments.Departments', on_delete=models.CASCADE, default=1)
    date_of_birth = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(default=1)

    class Meta:
        db_table = "tbl_employees"

    def create(self, validated_data):
        """
               Create and return a new `employees` instance, given the validated data.
               """
        return Employees.object.create(**validated_data)

    def update(self, instance, validate_data):
        """
               Update and return an existing `Employee` instance, given the validated data.
               """
        instance.first_name = validate_data.get("first_name", instance.first_name)
        instance.middle_name = validate_data.get("middle_name", instance.middle_name)
        instance.last_name = validate_data.get("last_name", instance.last_name)
        instance.phone_number = validate_data.get("phone_number", instance.phone_number)
        instance.id_number = validate_data.get("id_number", instance.id_number)
        instance.department_id = validate_data.get("department_id", instance.department_id)
        instance.date_of_birth = validate_data.get("date_of_birth", instance.date_of_birth)
        instance.created_at = validate_data.get("created_at", instance.created_at)
        instance.updated_at = validate_data.get("updated_at", instance.updated_at)
        instance.status = validate_data.get("status", instance.status)
        instance.save()
        return instance
