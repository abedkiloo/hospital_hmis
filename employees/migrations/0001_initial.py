# Generated by Django 2.1 on 2019-08-07 21:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('departments', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employees',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=255)),
                ('middle_name', models.CharField(max_length=255)),
                ('last_name', models.CharField(max_length=255)),
                ('phone_number', models.IntegerField()),
                ('id_number', models.IntegerField()),
                ('date_of_birth', models.CharField(max_length=255, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('status', models.IntegerField(default=1)),
                ('department', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='departments.Departments')),
            ],
            options={
                'db_table': 'tbl_employees',
            },
        ),
    ]
