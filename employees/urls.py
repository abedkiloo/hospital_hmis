from django.urls import path

from .views import EmployeesCreate, EmployeesList, EmployeesRetrieveUpdate, EmployeesDetail;

app_name = "employees"
urlpatterns = [
    path('employees/', EmployeesList.as_view(), name="employees-all"),
    path('employees/create/', EmployeesCreate.as_view(), name="employees-create"),
    path('employees/update/', EmployeesRetrieveUpdate.as_view(), name="employees-update"),
    path('employees/<int:pk>/', EmployeesDetail.as_view(), name="employees-detail"),
]
