from django.conf.urls import url

from .views import *

app_name = "patient_queue"

urlpatterns = [
    url(r'^patient-queues/$', patient_queue),
    url(r'^patient-queues/(?P<pk>[0-9]+)$', patient_queue_details),
]

# urlpatterns = [
#     path('patient-queues/', PatientQueueList.as_view(), name="patientqueues-all"),
#     path('patient-queues/create/', PatientQueueCreate.as_view(), name="patientqueues-create"),
#     path('patient-queues/update/', PatientQueueRetrieveUpdate.as_view(), name="patientqueues-update"),
#     path('patient-queues/<int:pk>/', PatientQueueDetail.as_view())
# ]
