# # Create your views here.
# from rest_framework import generics
#
# from .models import PatientQueue
# from .serailizers import PatientQueueSerializer, PatientQueueSaveSerializer
#
#
# class PatientQueueList(generics.ListAPIView):
#     """ View to list all PatientQueues"""
#     serializer_class = PatientQueueSerializer
#     queryset = PatientQueue.objects.all()
#
#
# class PatientQueueCreate(generics.CreateAPIView):
#     """ View to create a new PatientQueue. Only accepts POST requests """
#     queryset = PatientQueue.objects.all()
#     serializer_class = PatientQueueSaveSerializer
#
#
# class PatientQueueRetrieveUpdate(generics.RetrieveUpdateAPIView):
#     """ Retrieve a PatientQueue or update PatientQueue information.
#     Accepts GET and PUT requests and the record id must be provided in the request """
#     queryset = PatientQueue.objects.all()
#     serializer_class = PatientQueueSerializer
#
#
# class PatientQueueDetail(generics.RetrieveAPIView):
#     queryset = PatientQueue.objects.all()
#     serializer_class = PatientQueueSerializer


from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.datetime_safe import date
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serailizers import *


# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def patient_queue(request, version, ):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)
        patient_queue = PatientQueue.objects.order_by('created_at').reverse()
        # patient_queue = patient_queue.filter(created_at=date.today())
        paginator = Paginator(patient_queue, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = PatientQueueSerializer(data, context={"request": request}, many=True)
        # if data.JSON.parse(JSON.stringify(has_next())):
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = PatientQueueSaveSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def patient_queue_details(request, version, pk):
    try:
        patient_queue = PatientQueue.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = PatientQueueSerializer(patient_queue, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = PatientQueueSaveSerializer(patient_queue, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        patient_queue.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# class PatientsQueue(APIView):
#     queryset = PatientQueue.objects.all()
#
#     def get(self, request, *args, **kwargs):
#         patient_queue = PatientQueue.objects.all()
#         serializer = PatientQueueSerializer(patient_queue, many=True)
#         return Response({'patient-queue': serializer.data})
# JSON.parse(JSON.stringify(
#     # save patients
#     def post(self, request, *args, **kwargs):
#         patient_queue = request.data
#
#         serializer = PatientQueueSerializer(data=patient_queue)
#
#         if serializer.is_valid(raise_exception=True):
#             patient_queue_save = PatientQueue();
#             patient_queue.patient_id = Patients.objects.get(patient_queue.data.patients)
#             patient_queue.to_department_id=Departments.objects.get(patient_queue.data.to_deparment)
#             patient_queue.from_department_id=Departments.objects.get(patient_queue.data.from_department)
#             patient_queue.description=patient_queue.data.descrption
#             patient_queue.description=patient_queue.data.descrption
#             patient_queue_saved = serializer.save()
#         return Response({"success": "Patient Queue '{}' created queued to".format(patient_queue_saved.to_PatientQueue)})
