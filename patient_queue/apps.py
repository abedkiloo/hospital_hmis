from django.apps import AppConfig


class PatientQueueConfig(AppConfig):
    name = 'patient_queue'
