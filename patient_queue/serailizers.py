from departments.serializers import DepartmentsSerializer
from patients.serializers import PatientSerializer
from rest_framework import serializers

from .models import PatientQueue


class PatientQueueSaveSerializer(serializers.ModelSerializer):

    class Meta:
        model = PatientQueue

        fields = (
            'id', 'patient',  'from_department', 'to_department', 'description',
            'queue_status'
        )


class PatientQueueSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(read_only=True)
    from_department = serializers.PrimaryKeyRelatedField(read_only=True)
    to_department = serializers.PrimaryKeyRelatedField(read_only=True)
    description = serializers.CharField()
    queue_status = serializers.CharField()
    from_department = DepartmentsSerializer()
    to_department = DepartmentsSerializer()
    patient = PatientSerializer()

    class Meta:
        model = PatientQueue

        fields = (
            'id', 'patient', 'from_department', 'to_department', 'description',
            'queue_status','created_at'
        )
