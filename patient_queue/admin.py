from django.contrib import admin

from .models import PatientQueue
# Register your models here.
admin.site.register(PatientQueue)
