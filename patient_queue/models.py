from django.db import models


# Create your models here.
class PatientQueue(models.Model):
    patient = models.ForeignKey("patients.Patients", on_delete=models.CASCADE, related_name='queue_patient_id')
    from_department = models.ForeignKey("departments.Departments", on_delete=models.CASCADE,
                                        related_name='from_department_id')
    to_department = models.ForeignKey("departments.Departments", on_delete=models.CASCADE,
                                      related_name='to_department_id')
    # doctor_id = models.ForeignKey('doctors.Doctors', on_delete=models.CASCADE, related_name='assigned_doctor_id',
    #                               null=True, blank=True)
    # visit_id = models.IntegerField(default=1)
    description = models.CharField(max_length=255, null=True)
    queue_status = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True)

    class Meta:
        db_table = "tbl_patient_queue"
        ordering = ['created_at']
        get_latest_by='created_at'



    def __str__(self):
        return "{} -".format(self.id)
