from django.db import models


# Create your models here.
class Specimen(models.Model):
    specimen = models.CharField(max_length=255)
    unit = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "tbl_specimen"
        ordering=['id']
