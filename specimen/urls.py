from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^specimen/$', specimen),
    url(r'^specimen_details/(?P<pk>[0-9]+)$', specimen_details),
]
