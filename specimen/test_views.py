from unittest import TestCase

from django.test import Client
from django.urls import reverse

from ..models import Posts
from ..serializer import PostsSerializer


class GetAllPosts(TestCase):
    def setUp(self) -> None:
        Posts.objects.create(
            name="new post",
            description='new description',
            content="new content",
            post_type="post type"
        )
        Posts.objects.create(
            name="new post2",
            description='new description2',
            content="new content2",
            post_type="post type2"
        )
        self.client = Client()
        self.url = reverse("get_post_posts")

    def get_all_posts(self):
        posts = Posts.objects.all()
        serializer = PostsSerializer(posts, many=True)
        response = self.client.get(self.url)
        self.assertEqual(response.data.data_object, serializer.data)
