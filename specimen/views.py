# Create your views here.
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .models import Specimen
from .serializers import SpecimenSerailizer


@api_view(["GET", "POST"])
@permission_classes((permissions.AllowAny,))
def specimen(request, version):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)
        specimen = Specimen.objects.get_queryset().order_by('id')
        paginator = Paginator(specimen, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = SpecimenSerailizer(data, context={"request": request}, many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = SpecimenSerailizer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def specimen_details(request, version, pk):
    try:
        specimen = Specimen.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = SpecimenSerailizer(specimen, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = SpecimenSerailizer(specimen, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        patient.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
