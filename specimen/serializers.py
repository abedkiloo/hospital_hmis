from rest_framework import serializers

from .models import Specimen


class SpecimenSerailizer(serializers.ModelSerializer):
    class Meta:
        model = Specimen

        fields = (
            'id', 'specimen', 'unit', 'created_at',
        )
