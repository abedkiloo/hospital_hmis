# Generated by Django 2.1 on 2019-08-15 07:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('specimen', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='specimen',
            options={'ordering': ['id']},
        ),
    ]
