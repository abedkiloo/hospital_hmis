from django.apps import AppConfig


class RadiologyConfig(AppConfig):
    name = 'radiology'
