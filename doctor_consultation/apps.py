from django.apps import AppConfig


class DoctorConsultaionConfig(AppConfig):
    name = 'doctor_consultation'
