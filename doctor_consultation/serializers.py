from doctors.serializers import DoctorSerializer
from icd10_diagnosis.serializers import ICD10_diagnosisSerializer
from patients.serializers import PatientSerializer
from rest_framework import serializers

from .models import Diagnosis


class DiagnosisSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(read_only=True)
    patient = PatientSerializer()
    doctor = serializers.PrimaryKeyRelatedField(read_only=True)
    doctor = DoctorSerializer()
    preliminary_diagnosis = serializers.PrimaryKeyRelatedField(read_only=True)
    preliminary_diagnosis = ICD10_diagnosisSerializer()
    final_diagnosis = serializers.PrimaryKeyRelatedField(read_only=True)
    final_diagnosis = ICD10_diagnosisSerializer()

    class Meta:
        model = Diagnosis
        fields = (
            "id", "patient", "doctor", "presenting_complains", "preliminary_diagnosis", "final_diagnosis",
        )


class DiagnosisSaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Diagnosis
        fields = (
            "id", "patient", "doctor", "presenting_complains", "preliminary_diagnosis", "final_diagnosis",
        )
