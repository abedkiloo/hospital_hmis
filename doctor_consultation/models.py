from django.db import models


# Create your models here.
class Diagnosis(models.Model):
    patient = models.ForeignKey("patients.Patients", on_delete=models.CASCADE,
                                related_name='diagnosis_patient_id')
    doctor = models.ForeignKey("doctors.Doctors", on_delete=models.CASCADE,
                               related_name='diagnosis_doctor_id')
    presenting_complains = models.TextField(blank=True, null=True)
    preliminary_diagnosis = models.ForeignKey("icd10_diagnosis.ICD10_diagnosis", on_delete=models.CASCADE,
                                              related_name='preliminary_icd10_diagnosis_id')
    final_diagnosis = models.ForeignKey("icd10_diagnosis.ICD10_diagnosis", on_delete=models.CASCADE,
                                        related_name='final_icd10_diagnosis_id')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=False, null=True, blank=True)

    class Meta:
        db_table = "tbl_diagnosis"
