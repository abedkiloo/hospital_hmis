from django.urls import path

from .views import *

app_name = "doctor_consultation"
urlpatterns = [
    path('diagnosis/', patient_diagnosis),
    path('diagnosis/<int:pk>', patient_diagnosis_detail),

]
