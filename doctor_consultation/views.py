from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .models import Diagnosis
from .serializers import DiagnosisSaveSerializer, DiagnosisSerializer


# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def patient_diagnosis(request, version):
    """
    List all diagnosis
    """
    if request.method == 'GET':
        data = []
        nextPage = 1
        previousPage = 1
        diagnosis = Diagnosis.objects.all()
        page = request.GET.get('page', 1)
        paginator = Paginator(diagnosis, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = DiagnosisSerializer(data, context={'request': request}, many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    elif request.method == 'POST':
        serializer = DiagnosisSaveSerializer(data=request.data)
        if (serializer.is_valid()):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def patient_diagnosis_detail(request, version, pk):
    """
 Retrieve, update or delete a diagnosis by id/pk.
"""

    try:
        diagnosis = Diagnosis.objects.get(pk=pk)
    except Diagnosis.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = DiagnosisSerializer(diagnosis, context={'request': request})
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = DiagnosisSerializer(diagnosis, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        diagnosis.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
