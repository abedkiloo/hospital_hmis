from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^tests/$', tests),
    url(r'^tests/(?P<pk>[0-9]+)$', tests_details),
]
