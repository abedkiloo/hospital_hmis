from rest_framework import serializers

from .models import *


class TestsSerailizer(serializers.ModelSerializer):
    class Meta:
        model = Tests

        fields = (
            'id', 'test_name', 'test_description', 'created_at',
        )
