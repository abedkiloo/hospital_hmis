from django.db import models


# Create your models here.
class Tests(models.Model):
    test_name = models.CharField(max_length=255, blank=False, null=False)
    test_description = models.CharField(blank=True, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "tbl_tests"
