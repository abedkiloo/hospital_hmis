# from rest_framework import generics
#
# from .models import Departments
# from .serailizers import DepartmentsSerializer
#
#
# class DepartmentList(generics.ListAPIView):
#     """ View to list all Departments"""
#     queryset = Departments.objects.all().order_by('department_name')
#     serializer_class = DepartmentsSerializer
#
#
# class DepartmentCreate(generics.CreateAPIView):
#     """ View to create a new Department. Only accepts POST requests """
#     queryset = Departments.objects.all()
#     serializer_class = DepartmentsSerializer
#
#
# class DepartmentRetrieveUpdate(generics.RetrieveUpdateAPIView):
#     """ Retrieve a Department or update Department information.
#     Accepts GET and PUT requests and the record id must be provided in the request """
#     queryset = Departments.objects.all()
#     serializer_class = DepartmentsSerializer
#
#
# class DepartmentDetail(generics.RetrieveAPIView):
#     queryset = Departments.objects.all()
#     serializer_class = DepartmentsSerializer

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import *


# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def departments(request, version):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)
        departments = Departments.objects.all()
        """
            search for the department
        """
        if request.GET.get('search_key') is not None:
            departments = departments.filter(department_name__icontains=request.GET.get('search_key'))
        paginator = Paginator(departments, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = DepartmentsSerializer(data, context={"request": request}, many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = DepartmentsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def department_detail(request, version, pk):
    try:
        department = Departments.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = DepartmentsSerializer(department, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = DepartmentsSerializer(department, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        departments.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
