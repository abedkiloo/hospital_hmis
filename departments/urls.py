from django.conf.urls import url
from django.urls import path

from .views import *

app_name = "departments"

urlpatterns = [
    path('departments/', departments),
    url(r'^departments/(?P<pk>[0-9]+)$', department_detail),

]

# urlpatterns = [
#     path('departments/', DepartmentList.as_view(), name="departments-all"),
#     path('departments/create/', DepartmentCreate.as_view(), name="departments-create"),
#     path('departments/update/', DepartmentRetrieveUpdate.as_view(), name="departments-update"),
#     path('departments/<int:pk>', DepartmentDetail.as_view()),
# ]
