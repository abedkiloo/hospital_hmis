from django.db import models


# Create your models here.
class Departments(models.Model):
    department_name = models.CharField(max_length=255, null=False)
    description = models.CharField(max_length=255, null=False)

    class Meta:
        db_table = "tbl_departments"


    def __str__(self):
        return "{} ".format(self.department_name)
