# # Create your views here.
# from rest_framework import generics
#
# from .models import InpatientDetails
# from .serializers import InPatientDetailsSerializer, InPatientDetailsSaveSerializer
#
#
# class InPatientDetailsList(generics.ListAPIView):
#     """ View to list all Departments"""
#     queryset = InpatientDetails.objects.all().order_by('id')
#     serializer_class = InPatientDetailsSerializer
#
# class SingleInPatientDetails(generics.ListAPIView):
#     """ get details of signle patinets"""
#     model = InpatientDetails
#     serializer_class = InPatientDetailsSerializer
#
#     def get_queryset(self):
#         try:
#             patient_id = self.request.GET.get('patient_id')
#         except:
#             patient_id = ''
#         if (patient_id != ''):
#             object_list = self.model.objects.filter(patient_id=patient_id)
#
#         return object_list
#         # permission_classes = (permissions.IsAuthenticated,)
#
#
# class InPatientDetailsCreate(generics.CreateAPIView):
#     """ View to create a new Department. Only accepts POST requests """
#     queryset = InpatientDetails.objects.all()
#     serializer_class = InPatientDetailsSaveSerializer
#
#
# class InpatientDetailsRetrieveUpdate(generics.RetrieveUpdateAPIView):
#     """ Retrieve a Department or update Department information.
#     Accepts GET and PUT requests and the record id must be provided in the request """
#     queryset = InpatientDetails.objects.all()
#     serializer_class = InPatientDetailsSerializer
#
#
# class InpatientDetailstDetail(generics.RetrieveAPIView):
#     queryset = InpatientDetails.objects.all()
#     serializer_class = InPatientDetailsSerializer
#

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from patient_queue.models import PatientQueue
from patient_queue.serailizers import PatientQueueSaveSerializer
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import *


# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def in_patient_details(request, *args, **kwargs):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)
        in_patient_details = InpatientDetails.objects.all()

        """
            check whether the search of a patient is specified
        """
        if request.GET.get('patient_id') is not None:
            in_patient_details = in_patient_details.filter(patient_id=request.GET.get('patient_id'))
        paginator = Paginator(in_patient_details, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = InPatientDetailsSerializer(data, context={"request": request}, many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + kwargs.get('version') + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + kwargs.get('version') + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = InPatientDetailsSaveSerializer(data=request.data)

        if serializer.is_valid():
            """
                update queue details
            """
            queue_item = PatientQueue.objects.get(pk=request.data['queue_id'])
            data = {'queue_status': 1}
            serializer_patient_queue = PatientQueueSaveSerializer(queue_item, data=data, partial=True)
            if serializer_patient_queue.is_valid():
                serializer_patient_queue.save()
            """
                save the patient vitals
            """
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def in_patient_details_detail(request, **kwargs):
    try:
        department = InpatientDetails.objects.get(pk=kwargs.get('pk'))
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = InPatientDetailsSerializer(department, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = InPatientDetailsSerializer(department, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        in_patient_details.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

