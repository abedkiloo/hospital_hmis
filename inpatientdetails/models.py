from django.db import models


# Create your models here.
class InpatientDetails(models.Model):
    patient = models.ForeignKey("patients.Patients", on_delete=models.CASCADE,
                                related_name='inpatientdetails_patient_id')
    height = models.CharField(max_length=255, null=True)
    weight = models.CharField(max_length=255, null=True)
    blood_pressure = models.CharField(max_length=255)
    respiration = models.DecimalField(max_digits=5, decimal_places=3)
    known_allergies = models.CharField(max_length=255, blank=True)
    presenting_complains = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "tbl_in_patient_details"
