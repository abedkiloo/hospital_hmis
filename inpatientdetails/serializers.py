from patients.serializers import PatientSerializer
from rest_framework import serializers

from .models import InpatientDetails


class InPatientDetailsSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(read_only=True)

    patient = PatientSerializer()

    class Meta:
        model = InpatientDetails
        fields = (
            "id", "patient_id", "patient", "height", "weight", "blood_pressure", "known_allergies", "respiration"
            , "presenting_complains", "created_at"
        )



class InPatientDetailsSaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = InpatientDetails
        fields = (
            "id", "patient", "height", "weight", "blood_pressure", "known_allergies", "respiration"
            , "presenting_complains"
        )
