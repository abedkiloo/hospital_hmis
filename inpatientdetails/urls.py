from django.conf.urls import url

from .views import *

app_name = "inpatientdetails"

urlpatterns = [
    url(r'^patient_details$', in_patient_details),
    url(r'^patient_details/(?P<pk>[0-9]+)$', in_patient_details_detail),
]


# urlpatterns = [
#     path('patient_details/', InPatientDetailsList.as_view(), name="departments-all"),
#     path('patient_details/create/', InPatientDetai/lsCreate.as_view(), name="departments-create"),
#     path('patient_details/update/', InpatientDetailsRetrieveUpdate.as_view(), name="departments-update"),
#     path('patient_details/<int:pk>/', InpatientDetailstDetail.as_view()),
#     path('patient_details/detail/', SingleInPatientDetails.as_view()),
#
# ]
