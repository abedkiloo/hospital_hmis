from django.apps import AppConfig


class InpatientdetailsConfig(AppConfig):
    name = 'inpatientdetails'
