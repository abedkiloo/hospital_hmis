from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Patients


class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patients
        fields = (
            "id", "first_name", "middle_name", "last_name", "phone_number", "id_number",
            "date_of_birth","gender", "next_of_kin_name", "next_of_kin_phone_number", "relationship"
        )


class TokenSerializer(serializers.Serializer)   :
    """
     This serializer serializes the token data
     """
    token = serializers.CharField(max_length=255)


class UserSerializer(serializers.ModelSerializer):
    patients = serializers.PrimaryKeyRelatedField(many=True, queryset=Patients.objects.all())
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = User
        fields = ("id", "username", "email", "owner")
