from django.conf.urls import url

from .views import *

app_name = 'patients'

urlpatterns = [
    url(r'^patients/$', patients),
    url(r'^patients/(?P<pk>[0-9]+)$', patient_details),
]

# urlpatterns = [
#     path('patients/', ListPatientsView.as_view(), name="patients-all"),
#     path('patients/create/', PatientCreate.as_view(), name="patients-create"),
#     path('patients/update/', PatientRetrieveUpdate.as_view(), name="patients-update"),
#     path('patients/update/', PatientRetrieveUpdate.as_view(), name="patients-update"),
#     path('patients/search/', SearchPatientsView.as_view(), name="patients-update"),
#     path('patients/<int:pk>/', PatientDetail.as_view()),
#
# ]
