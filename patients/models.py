from django.db import models


# Create your models here.


class Patients(models.Model):
    first_name = models.CharField(max_length=255, null=True)
    middle_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    phone_number = models.CharField(max_length=255, null=True)
    gender = models.CharField(max_length=255, null=True)
    blood_group = models.CharField(max_length=255, null=True)
    address = models.CharField(max_length=255, null=True)
    note = models.CharField(max_length=255, null=True)
    id_number = models.IntegerField()
    date_of_birth = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(default=1)
    next_of_kin_name = models.CharField(max_length=255, null=True, default=1)
    next_of_kin_phone_number = models.CharField(max_length=255, null=True)
    relationship = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = "tbl_patients"

    def __str__(self):
        return "{} - {} ".format(self.first_name, self.middle_name)
