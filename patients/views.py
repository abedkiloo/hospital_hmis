# # Create your views here.
#
# from django.contrib.auth.models import User
# from rest_framework import generics, status
# from rest_framework import permissions
# from rest_framework.response import Response
# from rest_framework_jwt.settings import api_settings
#
# from .models import Patients
# from .serializers import PatientSerializer, UserSerializer
#
# # Get the JWT settings, add these lines after the import/from lines
# jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
# jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
#
#
# class ListPatientsView(generics.ListAPIView):
#     """
#     Provides a get method handler.
#     """
#     queryset = Patients.objects.all()
#     serializer_class = PatientSerializer
#     # permission_classes = (permissions.IsAuthenticated,)
#
#
# class SearchPatientsView(generics.ListAPIView):
#     """
#     Provides a get method handler.
#     """
#     model = Patients
#     serializer_class = PatientSerializer
#
#     def get_queryset(self):
#         try:
#             id_number = self.request.GET.get('id_number')
#         except:
#             id_number = ''
#         if (id_number != ''):
#             object_list = self.model.objects.filter(id_number__icontains=id_number)
#
#         return object_list
#         # permission_classes = (permissions.IsAuthenticated,)
#
#
# class PatientCreate(generics.CreateAPIView):
#     """ View to create a new user. Only accepts POST requests """
#     queryset = Patients.objects.all()
#     serializer_class = PatientSerializer
#     # permission_classes = (permissions.IsAdminUser,)
#
#
# class PatientRetrieveUpdate(generics.RetrieveUpdateAPIView):
#     """ Retrieve a user or update user information.
#     Accepts GET and PUT requests and the record id must be provided in the request """
#     queryset = Patients.objects.all()
#     serializer_class = PatientSerializer
#     # permission_classes = (permissions.IsAuthenticated, )
#
#
# class UserList(generics.ListAPIView):
#     def perform_create(self, serializer):
#         serializer.save(owner=self.request.user)
#
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#     permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
#
#
# class PatientDetail(generics.RetrieveAPIView):
#     queryset = Patients.objects.all()
#     serializer_class = PatientSerializer
#
#
# class UserDetail(generics.RetrieveAPIView):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
#
#
# class RegisterUsers(generics.CreateAPIView):
#     """
#     POST auth/register/
#     """
#     permission_classes = (permissions.AllowAny,)
#
#     def post(self, request, *args, **kwargs):
#         username = request.data.get("username", "")
#         password = request.data.get("password", "")
#         email = request.data.get("email", "")
#         if not username and not password and not email:
#             return Response(
#                 data={
#                     "message": "username, password and email is required to register a user"
#                 },
#                 status=status.HTTP_400_BAD_REQUEST
#             )
#         new_user = User.objects.create_user(
#             username=username, password=password, email=email
#         )
#         return Response(
#             data=UserSerializer(new_user).data,
#             status=status.HTTP_201_CREATED
#         )


from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import *


# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def patients(request, version, ):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)
        patients = Patients.objects.all()

        if request.GET.get('id_number') is not None:
            patients = patients.filter(id_number__icontains=request.GET.get('id_number'))
        paginator = Paginator(patients, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = PatientSerializer(data, context={"request": request}, many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = PatientSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def patient_details(request, version, pk):
    try:
        patient = Patients.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = PatientSerializer(patient, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = PatientSerializer(patient, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        patient.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
