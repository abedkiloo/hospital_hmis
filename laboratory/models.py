from django.db import models


# Create your models here.
# Create your models here.
class Lab(models.Model):
    patient = models.ForeignKey("patients.Patients", on_delete=models.CASCADE, related_name='queue_patient_id')
    from_department = models.ForeignKey("departments.Departments", on_delete=models.CASCADE,
                                        related_name='from_department_id')
    # specimen_id=
    label = models.CharField(max_length=255, null=True)
    status = models.IntegerField(blank=True)
    comment = models.CharField(max_length=255)
    acceptance = models.IntegerField()
    rejection_reason = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
