from django.db import models


class Users(models.Model):
    employee=models.ForeignKey("employees.Employees",on_delete=models.CASCADE, default=1)
    username = models.CharField(max_length=255, null=True)
    password = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(default=1)

    class Meta:
        db_table = "tbl_users"
