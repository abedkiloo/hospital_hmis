from rest_framework import serializers

from .models import ICD10_diagnosis


class ICD10_diagnosisSerializer(serializers.ModelSerializer):
    class Meta:
        model = ICD10_diagnosis
        fields = (
            "id", "code", "diagnosis", "description",
            "created_at", "updated_at"
        )
