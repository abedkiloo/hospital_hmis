from django.db import models


# Create your models here.
class ICD10_diagnosis(models.Model):
    code = models.CharField(max_length=255, blank=False, null=False)
    diagnosis = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=False, null=True, blank=True)

    class Meta:
        db_table = "tbl_icd10_diagnosis"
