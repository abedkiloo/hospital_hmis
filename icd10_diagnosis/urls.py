from django.urls import path

from .views import *

app_name = "icd10_diagnosis"

urlpatterns = [
    path('icd10_diagnosis/', icd10_diagnosis),
]
