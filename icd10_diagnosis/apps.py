from django.apps import AppConfig


class Icd10DiagnosisConfig(AppConfig):
    name = 'icd10_diagnosis'
