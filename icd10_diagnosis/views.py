from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import *


# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def icd10_diagnosis(request, version):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)
        icd10_diagnosis = ICD10_diagnosis.objects.all()
        paginator = Paginator(icd10_diagnosis, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = ICD10_diagnosisSerializer(data, context={"request": request}, many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = ICD10_diagnosisSerializer(data=request.data)
        if (serializer.is_valid()):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
