"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [

    path('', admin.site.urls),

    path('api-token-auth/', obtain_jwt_token, name='create-token'),
    re_path('api/(?P<version>(v1|v2))/', include('users.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('patients.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('doctors.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('departments.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('employees.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('patient_queue.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('inpatientdetails.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('doctor_consultation.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('icd10_diagnosis.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('specimen.urls')),
    re_path('api/(?P<version>(v1|v2))/', include('tests.urls'))
]
