from django.db import models


# Create your models here.


class Doctors(models.Model):
    first_name = models.CharField(max_length=255, null=True)
    middle_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=255, null=True)
    username = models.CharField(max_length=255, null=True)
    password = models.CharField(max_length=255, null=True)
    id_number = models.CharField(max_length=255, null=True)
    phone_number = models.CharField(max_length=255, null=True)
    specialization = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = "tbl_doctors"

    def __str__(self):
        return "{} - {} ".format(self.first_name, self.middle_name)


class DoctorNotes(models.Model):
    doctor = models.ForeignKey(Doctors, on_delete=models.CASCADE, related_name='doctor_notes_doctor_id')
    patient = models.ForeignKey("patients.Patients", on_delete=models.CASCADE, related_name='doctor_notes_patient_id')
    notes = models.CharField(max_length=1000)
    diagnosis = models.CharField(max_length=1000, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "tbl_doctors_notes"


class NoteDiagnosis(models.Model):
    patient = models.ForeignKey("patients.Patients", on_delete=models.CASCADE, related_name='notes_diagnosis_patient_id')
    note = models.ForeignKey(DoctorNotes, on_delete=models.CASCADE, related_name='notes_diagnosis_note_id')
    diagnosis = models.ForeignKey('doctor_consultation.Diagnosis', on_delete=models.CASCADE, related_name='notes_diagnosis_diagnosis_id')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        db_table = "tbl_notes_diagnosis"
