from django.conf.urls import url

from .views import *

app_name = "doctors"

urlpatterns = [
    url(r'^doctors/$', doctors),
    url(r'^doctors/(?P<pk>[0-9]+)$', doctor_detail),

    url(r'^doctor-notes/$', doctor_notes),
    url(r'^doctor-notes/(?P<pk>[0-9]+)$', doctor_note_detail),

    url(r'^note_diagnosis/$', note_diagnosis),
    url(r'^note_diagnosis/(?P<pk>[0-9]+)$', note_diagnosis_detail),

]

# urlpatterns = [
#     path('doctors/', DoctorList.as_view(), name="doctors-all"),
#     path('doctors/create/', DoctorCreate.as_view(), name="doctors-create"),
#     path('doctors/update/', DoctorRetrieveUpdate.as_view(), name="doctors-update"),
#     path('doctors/<int:pk>', DoctorDetail.as_view(), name="doctor-detail"),
#
#     path('doctors-notes/create/', DoctorNotesCreate.as_view(), name="doctor-notes-create"),
#
# ]
