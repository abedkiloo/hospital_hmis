from doctor_consultation.models import Diagnosis
from icd10_diagnosis.serializers import ICD10_diagnosisSerializer
from patients.serializers import PatientSerializer
from rest_framework import serializers

from .models import Doctors, NoteDiagnosis, DoctorNotes


class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctors

        fields = (
            'id', 'first_name', 'last_name', 'username',
            'password', 'phone_number', 'specialization', 'id_number'
        )

    def validate_phone(self, value):
        qs = Doctors.objects.filter(phone_number__iexact=value)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)
        if qs.exists():
            return serializers.ValidationError("Phone Number already exists")
        return value


class DoctorNotesSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(read_only=True)
    patient = PatientSerializer()
    doctor = serializers.PrimaryKeyRelatedField(read_only=True)
    doctor = DoctorSerializer()

    class Meta:
        model = DoctorNotes

        fields = (
            'id', 'patient', 'doctor', 'notes', 'diagnosis',
        )

class DoctorNotesSaveSerializer(serializers.ModelSerializer):

    class Meta:
        model = DoctorNotes

        fields = (
            'id', 'patient', 'doctor', 'notes', 'diagnosis',
        )


class NotesDiagnosisSaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoteDiagnosis

        fields = (
            'id', 'patient', 'note', 'diagnosis', 'created_at'
        )


class DiagnosisSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(read_only=True)
    patient = PatientSerializer()
    doctor = serializers.PrimaryKeyRelatedField(read_only=True)
    doctor = DoctorSerializer()
    preliminary_diagnosis = serializers.PrimaryKeyRelatedField(read_only=True)
    preliminary_diagnosis = ICD10_diagnosisSerializer()
    final_diagnosis = serializers.PrimaryKeyRelatedField(read_only=True)
    final_diagnosis = ICD10_diagnosisSerializer()

    class Meta:
        model = Diagnosis
        fields = (
            "id", "patient", "doctor", "presenting_complains", "preliminary_diagnosis", "final_diagnosis",
        )


class NotesDiagnosisSerializer(serializers.ModelSerializer):
    patient = serializers.PrimaryKeyRelatedField(read_only=True)
    patient = PatientSerializer()
    note = serializers.PrimaryKeyRelatedField(read_only=True)
    note = DoctorNotesSerializer()
    diagnosis = serializers.PrimaryKeyRelatedField(read_only=True)
    diagnosis = DiagnosisSerializer()

    class Meta:
        model = NoteDiagnosis

        fields = (
            'id', 'note', 'patient', 'diagnosis', 'created_at'
        )
