# from django.db.models import Q
# from rest_framework import generics
#
# from .models import Doctors, DoctorNotes
# from .serializers import DoctorSerializer, DoctorNotesSerializer
#
#
# class DoctorList(generics.ListAPIView):
#     """ View to list all Doctors"""
#     queryset = Doctors.objects.all().order_by('first_name')
#     serializer_class = DoctorSerializer
#
#     def get_queryset(self):
#         qs = Doctors.objects.all()
#         query = self.request.GET.get("q")
#         if query is not None:
#             qs = qs.filter(Q(first_name__icontains=query) | Q(middle_name__icontains=query)).distinct()
#         return qs
#
#
# class DoctorCreate(generics.CreateAPIView):
#     """ View to create a new Doctor. Only accepts POST requests """
#     queryset = Doctors.objects.all()
#     serializer_class = DoctorSerializer
#
#
# class DoctorRetrieveUpdate(generics.RetrieveUpdateAPIView):
#     """ Retrieve a Doctor or update Doctor information.
#     Accepts GET and PUT requests and the record id must be provided in the request """
#     queryset = Doctors.objects.all()
#     serializer_class = DoctorSerializer
#
#
# class DoctorDetail(generics.RetrieveAPIView):
#     queryset = Doctors.objects.all()
#     serializer_class = DoctorSerializer
#
#
# class DoctorNotesCreate(generics.CreateAPIView):
#     queryset = DoctorNotes.objects.all()
#     serializer_class = DoctorNotesSerializer


from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from .serializers import *


# Create your views here.
@api_view(['GET', 'POST'])
@permission_classes((permissions.AllowAny,))
def doctors(request, version):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)
        doctors = Doctors.objects.all()
        paginator = Paginator(doctors, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = DoctorSerializer(data, context={"request": request}, many=True)
        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = DoctorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def doctor_detail(request, version, pk):
    try:
        doctor = Doctors.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = DoctorSerializer(doctor, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = DoctorSerializer(doctor, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        doctor.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


"""
doctor Notes here
"""


@api_view(["GET", "POST"])
@permission_classes((permissions.AllowAny,))
def doctor_notes(request, version):
    if request.method == "POST":

        data_values = {}
        all_diagnosis = {}
        # ToDO Update to include many diagnosis selection
        for (key, values) in request.data.items():
            data_value = {key: values}
            # return Response(values)
            if key == "diagnosis":
                for val in values:
                    data_value[key] = val
                    data_values.update(data_value)
            data_values.update(data_value)

        serializer = DoctorNotesSaveSerializer(data=data_values)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)

        doctor_notes = DoctorNotes.objects.all()

        paginator = Paginator(doctor_notes, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = DoctorNotesSerializer(data, context={"request": request}, many=True)

        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/doctor-notes/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/doctor-notes/?page=' + str(previousPage)})


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def doctor_note_detail(request, version, pk):
    try:
        doctor_note = DoctorNotes.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = NotesDiagnosisSerializer(doctor_note, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = NotesDiagnosisSerializer(doctor_note, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        doctor_note.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


"""
attach diagnosis to a note
"""


@api_view(["GET", "POST"])
@permission_classes((permissions.AllowAny,))
def note_diagnosis(request, version):
    if request.method == "GET":

        nextPage = 1
        previousPage = 1
        page = request.GET.get('page', 1)

        note_diagnosis = NoteDiagnosis.objects.all()

        """
            get the diagnosis of a single patient
        """
        # ToDo map user with note and remove redundant field and search how to query many through
        if request.GET.get('patient_id') is not None:
            note_diagnosis = note_diagnosis.filter(patient=request.GET.get('patient_id'))

        paginator = Paginator(note_diagnosis, 5)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        serializer = NotesDiagnosisSerializer(data, context={"request": request}, many=True)

        if data.has_next():
            nextPage = data.next_page_number()
        if data.has_previous():
            previousPage = data.previous_page_number()

        return Response({'data': serializer.data, 'count': paginator.count, 'numpages': paginator.num_pages,
                         'nextlink': '/api/' + version + '/diagnosis/?page=' + str(nextPage),
                         'prevlink': '/api/' + version + '/diagnosis/?page=' + str(previousPage)})

    if request.method == "POST":
        serializer = NotesDiagnosisSaveSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.AllowAny,))
def note_diagnosis_detail(request, version, pk):
    try:
        note_diagnosis = NoteDiagnosis.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == "GET":
        serializer = NotesDiagnosisSerializer(note_diagnosis, context={"request": request})
        return Response({
            "status": True,
            "data": serializer.data})

    elif request.method == 'PUT':
        serializer = NotesDiagnosisSerializer(note_diagnosis, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        note_diagnosis.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
